package com.saltboxgames.latticelight.utilities;

@FunctionalInterface
public interface IConsoleCommand 
{
	public void run(String[] args);
}