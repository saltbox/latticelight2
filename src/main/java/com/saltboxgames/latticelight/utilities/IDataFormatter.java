package com.saltboxgames.latticelight.utilities;

@FunctionalInterface
public interface IDataFormatter 
{
	String[] format(Object object);
}
