package com.saltboxgames.latticelight.utilities;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.joml.Vector3d;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

public class Utilities
{
	public static boolean canParseInt(String str)
	{
		return str.matches("^-?\\d+$");
	}
	
	public static void Vec3dToVec3f(Vector3d in, Vector3f out)
	{
		out.x = (float) in.x;
		out.y = (float) in.y;
		out.z = (float) in.z;
	}
	
	// Buffer Utils
	public static FloatBuffer createFlippedBuffer(float[] data)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static ByteBuffer createFlippedBuffer(byte[] data)
	{
		ByteBuffer buffer = BufferUtils.createByteBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static ShortBuffer createFlippedBuffer(short[] data)
	{
		ShortBuffer buffer = BufferUtils.createShortBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static IntBuffer createFlippedBuffer(int[] data)
	{
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	//This is dirty, but it's faster for what we're doing.
	public static byte[] stringToBytesASCII(String str) 
	{
		 byte[] buffer = new byte[str.length()];
		 for (int i = 0; i < buffer.length; i++) 
		 {
			 buffer[i] = (byte) str.charAt(i);
		 }
		 return buffer;
	}
}
