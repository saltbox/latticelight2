package com.saltboxgames.latticelight.utilities;

import org.joml.Vector3f;

import com.saltboxgames.latticelight.Game;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.enginetools.EngineTool;
import com.saltboxgames.latticelight.rendering.Graphics;
import com.saltboxgames.latticelight.rendering.TileMesh;

public class FrameCounter extends EngineTool 
{
	private TileMesh mesh;
	private Game game;
	
	private float time;
	private int frameCount = 0;
	
	private Vector3f position;
	
	private static final Vector3f[] colors = new Vector3f[]{
		new Vector3f(1, 0, 0),	// bad
		new Vector3f(1, 1, 0),	// ok
		new Vector3f(0, 1, 0)	// good
	};
	
	public FrameCounter(Game game, Vector3f position) 
	{
		this.game = game;
		this.position = position;
		this.mesh = new TileMesh(8, 1, 16, 16);
	}
	
	@Override 
	public void onUpdate(GameTime gameTime) 
	{
		frameCount ++;
		
		time += gameTime.getDeltaTime();
		if(time >= 1)
		{
			String msg = "FPS " + frameCount;
			
			int frameThird = (int) Math.floor(((float)frameCount / game.getTickRate()) * 2);
			if(frameThird > 2)
			{
				frameThird = 2;
			}
			
			for (int i = 0; i < mesh.getWidth(); i++) 
			{
				if(i < msg.length())
				{
					mesh.bufQuadTile(i, 0, (byte)msg.charAt(i));
					if(i > 3)
					{
						mesh.bufQuadColor(i, 0, colors[frameThird]);
					}
				}
				else 
				{
					mesh.bufQuadTile(i, 0, (byte)0);
				}
			}
			frameCount = 0;
			time -= 1;
			mesh.pushBuffers();
		}
	}
	
	@Override
	public void onDraw(GameTime gameTime, Graphics graphics) 
	{
		graphics.Draw(mesh, position);
	}
}
