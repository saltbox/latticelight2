package com.saltboxgames.latticelight.logging;

import com.saltboxgames.latticelight.Game;

public class SystemConsole implements ILoggable
{
	private Game game;
	
	public SystemConsole(Game game)
	{
		this.game = game;
	}
	
	public void writeLine(LogMessage message) 
	{
		switch (message.getType()) 
		{
		case Logger.LOG_STACK_TRACE:
		case Logger.LOG_ERROR:
			System.err.println(message);
			break;
		default:
			System.out.println(message);
			break;
		}
	}
}
