package com.saltboxgames.latticelight.logging;

public interface ILoggable  
{
	public void writeLine(LogMessage message);
}
