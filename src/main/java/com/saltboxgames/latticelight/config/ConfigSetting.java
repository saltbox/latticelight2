package com.saltboxgames.latticelight.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigSetting
{
	public String name() default "";
}
