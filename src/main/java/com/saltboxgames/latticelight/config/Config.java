package com.saltboxgames.latticelight.config;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.joml.Vector2i;
import org.json.JSONArray;
import org.json.JSONObject;

import com.saltboxgames.latticelight.logging.Logger;

public class Config 
{
	static HashMap<Class<?>, LoadTypeHandler<?>> loadDispatch = new HashMap<>();
	static HashMap<Class<?>, SaveTypeHandler> saveDispatch = new HashMap<>();
	
	// Please have mercy..
	// -- Config Setting Loading
	public static boolean loadSettings(Object destination, String path)
	{	
		try 
		{
			Path filePath = Paths.get(path);
			System.out.println(filePath.toAbsolutePath());
			List<String> data = Files.readAllLines(filePath);
			
			StringBuilder builder = new StringBuilder();
			builder.append("{ ");
			for (String line : data) 
			{
				if(line.length() > 0 && !line.startsWith("#"))
				{
					builder.append(line).append(",");
				}
			}
			builder.append(" }");
			
			Logger.writeLine("Loading Settings " + path, Logger.LOG_DEBUG);

			JSONObject jsonObject = new JSONObject(builder.toString());
			
			ArrayList<Field> allFields = new ArrayList<Field>();			
			allFields.addAll(Arrays.asList(destination.getClass().getDeclaredFields()));
			
			
			Class<?> superClass = destination.getClass().getSuperclass();
			while (superClass != null && superClass != Object.class) 
			{
				for (Field field : superClass.getDeclaredFields()) {
		            if (!field.isSynthetic()) {
		            	allFields.add(field);
		            }
		        }
				superClass = superClass.getSuperclass();
			}
						
			// Evil Evil Evil Code (Please Forgive me)
			for(Field field : allFields)
			{
				ConfigSetting configSetting = field.getAnnotation(ConfigSetting.class);
				if(configSetting != null)
				{
					String fieldName = configSetting.name();
					if(fieldName.length() == 0)
					{
						fieldName = field.getName().toLowerCase();
					}
					
					if((field.getModifiers() & Modifier.FINAL) == Modifier.FINAL)
					{
						Logger.writeLine("Could not load " + fieldName + " variables has final modifier", Logger.LOG_ERROR);
						break;
					}
					
					if(jsonObject.has(fieldName))
					{
						if((field.getModifiers() & Modifier.PRIVATE) == Modifier.PRIVATE)
						{
							field.setAccessible(true);
						}
						
						LoadTypeHandler<?> loadHandler = loadDispatch.get(field.getType());
						if(loadHandler != null)
						{
							Object loadVal = loadHandler.onLoad(jsonObject.get(fieldName));					
							Logger.writeLine(fieldName + ":" + loadVal, Logger.LOG_DEBUG);
							if(loadVal != null)
							{
								try 
								{
									field.set(destination, loadVal);
								} 
								catch (IllegalArgumentException e) 
								{
									Logger.writeLine(e);
									return false;
								} 
								catch (IllegalAccessException e) 
								{
									Logger.writeLine(e);
									return false;
								}
							}
							else 
							{
								Logger.writeLine("Error reading value: " + fieldName, Logger.LOG_ERROR);
							}
						}
						else 
						{
							//TODO: Type not in Dispatcher
							Logger.writeLine(field.getType() + " not found in load dispatcher", Logger.LOG_ERROR);
						}
					}
					else 
					{
						//TODO: Field not in file
						Logger.writeLine(fieldName + " not found in " + path, Logger.LOG_ERROR);
					}
				}
			}
			
			return true;
		} 
		catch (IOException e) 
		{
			Logger.writeLine(e);
		}
		catch (Exception e) 
		{
			Logger.writeLine("FAILED TO LOAD CONFIG", Logger.LOG_ERROR);
			Logger.writeLine(e);
		}
		return false;
	}
	
	
	
	static
	{
		initDefaultLoadHandlers();
		initDefaultSaveHandlers();
	}

	private static void initDefaultLoadHandlers()
	{
		// Vector2i
		loadDispatch.put(Vector2i.class, (Object val) ->{
			if(val instanceof JSONArray)
			{
				JSONArray array = (JSONArray) val;
				if(array.length() == 2)
				{
					if(array.get(0) instanceof Integer && array.get(1) instanceof Integer)
					{
						return new Vector2i(array.getInt(0), array.getInt(1));
					}
				}
			}
			return null;
		});
		
		// int
		loadDispatch.put(int.class, (Object val) ->{
			if(val instanceof Integer)
			{
				return (int) val;
			}
			return null;
		});
		
		// double
		loadDispatch.put(double.class, (Object val) ->{
			if(val instanceof Integer)
			{
				return (double) val;
			}
			return null;
		});
	}
	
	private static void initDefaultSaveHandlers()
	{
		
	}
}
