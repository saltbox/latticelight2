package com.saltboxgames.latticelight.config;

//Spooky Devil Magic
@FunctionalInterface
public interface LoadTypeHandler<T> {
	T onLoad(Object JsonVal);
}
