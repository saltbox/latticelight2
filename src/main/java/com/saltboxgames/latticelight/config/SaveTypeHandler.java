package com.saltboxgames.latticelight.config;

@FunctionalInterface
public interface SaveTypeHandler 
{
	Object onSave(Object obj);
}
