package com.saltboxgames.latticelight.input;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFWKeyCallback;

public final class KeyboardInput extends GLFWKeyCallback
{
	//--Static--
	private static KeyboardInput instance;
	private static boolean[] currentKeys = new boolean[65536], lastKeys = new boolean[65536];
	private static ArrayList<IKeyCallback> keyCallbacks = new ArrayList<>();
	
	private static boolean anyKey, lastAnyKey; 
	
	public static KeyboardInput getInstance()
	{
		if(instance == null)
		{
			instance = new KeyboardInput();
		}
		return instance;
	}
	
	public static boolean isKeyDown(int keyCode)
	{
		if(keyCode < 0)
		{
			return anyKey;
		}
		else 
		{
			return currentKeys[keyCode];
		}
	}
	
	public static boolean isKeyDown(int keyCode, boolean lastFrame)
	{
		
		if(keyCode < 0)
		{
			if(lastFrame)
			{
				return lastAnyKey;
			}
			return anyKey;
		}
		else 
		{
			if(lastFrame)
			{
				return lastKeys[keyCode];
			}
			return currentKeys[keyCode];
		}
	}
	
	public static void Update()
	{
		//This should be the fastest way to copy an array?
		System.arraycopy(currentKeys, 0, lastKeys, 0, currentKeys.length);	
		lastAnyKey = anyKey;
		anyKey = false;
	}
	
	public static void registerKeyCallBack(IKeyCallback callback)
	{
		if(!keyCallbacks.contains(callback))
		{
			keyCallbacks.add(callback);
		}
	}
	
	//--
	private KeyboardInput() 
	{		
	}
	
	@Override
	public void invoke(long window, int key, int scancode, int action, int mods) 
	{
		if(key >= 0 && key <= 65535)
		{
			currentKeys[key] = (action != GLFW_RELEASE);
			anyKey = (action != GLFW_RELEASE);
		}
		for (IKeyCallback iKeyCallback : keyCallbacks) {
			iKeyCallback.keyCallback(key, scancode, action, mods);
		}	
	}	
}
