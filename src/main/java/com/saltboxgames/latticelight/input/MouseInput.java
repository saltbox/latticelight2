package com.saltboxgames.latticelight.input;

import static org.lwjgl.glfw.GLFW.*;

import org.joml.Vector2f;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

public class MouseInput 
{
	//--Static--
	private static MouseInput instance; 
	private static Vector2f scrollDelta;
	private static Vector2f cursorPos;
	
	//--buttons
	private static boolean[] currentButtons = new boolean[10]; // <-- 10 should be enough?
	private static boolean[] lastButtons = new boolean[10];
	
	public static MouseInput getInstance() 
	{
		if(instance == null)
		{
			instance = new MouseInput();
			cursorPos = new Vector2f();
			scrollDelta = new Vector2f();
		}
		
		
		return instance;
	}
	
	public static Vector2f getPos(Vector2f out)
	{
		out.x = cursorPos.x;
		out.y = cursorPos.y;
		return out;
	}
	
	public static boolean isButtonDown(int buttonCode)
	{
		return currentButtons[buttonCode];
	}
	
	public static boolean isButtonDown(int buttonCode, boolean lastFrame)
	{
		if(lastFrame)
		{
			return lastButtons[buttonCode];
		}
		return currentButtons[buttonCode];
	}
	
	public static Vector2f getScrollDelta(Vector2f out)
	{
		out.x = scrollDelta.x;
		out.y = scrollDelta.y;
		return out;
	}
	
	public static float getVerticalScrollDelta()
	{
		return scrollDelta.y;
	}
	
	public static float getHorizontalScrollDelta()
	{
		return scrollDelta.x;
	}
	
	public static void Update()
	{
		System.arraycopy(currentButtons, 0, lastButtons, 0, currentButtons.length);
		scrollDelta.zero();
	}
	
	//--
	private CursorPosCallback cp;
	private MouseButtonCallback mb;
	private ScrollCallback s;
	
	private MouseInput() 
	{
		cp = new CursorPosCallback();
		mb = new MouseButtonCallback();
		s = new ScrollCallback();
	}
	
	public void registerCallBacks(long window)
	{
		glfwSetCursorPosCallback(window, cp);
		glfwSetMouseButtonCallback(window, mb);
		glfwSetScrollCallback(window, s);
	}
	
	//--Internal Classes
	class CursorPosCallback extends GLFWCursorPosCallback
	{
		@Override
		public void invoke(long window, double xpos, double ypos) {
			cursorPos.x = (float) xpos;
			cursorPos.y = (float) ypos;
		}		
	}
	class MouseButtonCallback extends GLFWMouseButtonCallback
	{
		@Override
		public void invoke(long window, int button, int action, int mods) {
			currentButtons[button] = (action != GLFW_RELEASE);
		}
	}
	class ScrollCallback extends GLFWScrollCallback
	{
		@Override
		public void invoke(long window, double xoffset, double yoffset) {
			scrollDelta.x = (float) xoffset;
			scrollDelta.y = (float) yoffset;
		}		
	}
}
