package com.saltboxgames.latticelight.input;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.glfw.GLFWCharCallback;

public class CharCallbackHandler extends GLFWCharCallback 
{
	private static CharCallbackHandler instance;
	private static List<ICharCallback> callbacks = new ArrayList<ICharCallback>();
	
	
	public static void registerCallBack(ICharCallback callback)
	{
		callbacks.add(callback);
	}
	
	public static CharCallbackHandler getInstance() 
	{
		if(instance == null)
		{
			instance = new CharCallbackHandler();
		}
		return instance;
	}
	
	private CharCallbackHandler() 
	{
		
	}
	
	@Override
	public void invoke(long window, int codepoint) 
	{
		for (ICharCallback charCallback : callbacks) 
		{
			charCallback.charPressed(codepoint);
		}
	}
}
