package com.saltboxgames.latticelight.rendering;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.FloatBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import com.saltboxgames.latticelight.logging.Logger;

public class Shader 
{	
	private static final int 
		UNI_MODEL_MATRIX = 0,
		UNI_VIEW_MATRIX = 1,
		UNI_PROJ_MATRIX = 2,
		UNI_TEXTURE1 = 3;
	
	protected int programID;
	private int vertexShaderID, fragmentShaderID;
	
	private int[] typical_uniforms;
	
	private Shader(int vertexShader, int fragmentShader)
	{
		this.vertexShaderID = vertexShader;
		this.fragmentShaderID = fragmentShader;
		
		programID = glCreateProgram();
		glAttachShader(programID, vertexShaderID);
		glAttachShader(programID, fragmentShaderID);
		
		glBindAttribLocation(programID, 0, "in_position"); 
        glBindAttribLocation(programID, 1, "in_fore_color");
        glBindAttribLocation(programID, 2, "in_back_color"); 
        glBindAttribLocation(programID, 3, "in_texCoord0");
		
		glLinkProgram(programID);
		glValidateProgram(programID);
		
		//Cache typical uniform variables
		typical_uniforms = new int[]
		{
			getUniform("model_matrix"), 	// 	UNI_MODEL_MATRIX;
			getUniform("view_matrix"),		//	UNI_VIEW_MATRIX;
			getUniform("proj_matrix"),		//	UNI_PROJ_MATRIX;
			getUniform("texture1")			//  UNI_TEXTURE1;
		};
	}
	
	public int getUniform(String name)
	{
		int result = glGetUniformLocation(programID, name);
		if(result == -1)
		{
			Logger.writeLine("Could not find uniform \'" + name + "\' in shader program: " + programID, Logger.LOG_ERROR);
		}
		return result;
	}
	
	
	public void setUniform_f(int uniformID, float f)
	{
		glUniform1f(uniformID, f);		
	}
	
	public void setUniform_f(String name, float f)
	{
		glUniform1f(getUniform(name), f);
	}
	
	public void setUniform_v3f(int uniformID, Vector3f vec3)
	{
		glUniform3f(uniformID, vec3.x, vec3.y, vec3.z);
	}
	
	public void setUniform_v3f(String name, Vector3f vec3)
	{
		glUniform3f(getUniform(name), vec3.x, vec3.y, vec3.z);
	}
	
	public void setUniform_v2f(int uniformID, Vector2f vec2)
	{
		glUniform2f(uniformID, vec2.x, vec2.y);
	}
	
	public void setUniform_v2f(String name, Vector2f vec2)
	{
		glUniform2f(getUniform(name), vec2.x, vec2.y);
	}
	
	private FloatBuffer matrixfb = BufferUtils.createFloatBuffer(16);
	public void Update(Matrix4f projMatrix, Matrix4f viewMatrix, Matrix4f modelMatrix)
	{
		viewMatrix.get(matrixfb);
		glUniformMatrix4fv(typical_uniforms[UNI_VIEW_MATRIX],  false, matrixfb);
		matrixfb.clear();
		
		projMatrix.get(matrixfb);
		glUniformMatrix4fv(typical_uniforms[UNI_PROJ_MATRIX],  false, matrixfb);
		matrixfb.clear();
		
		modelMatrix.get(matrixfb);
		glUniformMatrix4fv(typical_uniforms[UNI_MODEL_MATRIX], false, matrixfb);
		matrixfb.clear();	
	}
	
	public void setTexture1(int TextureNum)
	{
		glUniform1i(typical_uniforms[UNI_TEXTURE1], TextureNum);
	}
	
	public void bind()
	{
		glUseProgram(programID);
	}
	
	public void unbind()
	{
		glUseProgram(0);
	}
	
	//TODO: Write Finalize to clean up GPU Ram
	
	
	//-- Static Functions
	private static int compileShader(String shaderString, int type)
	{
		int shaderID = glCreateShader(type);
		glShaderSource(shaderID, shaderString);
		glCompileShader(shaderID);
		if(glGetShaderi(shaderID, GL_COMPILE_STATUS) == GL_FALSE)
		{
			Logger.writeLine("Could not compile shader " + shaderID);
			Logger.writeLine(glGetShaderInfoLog(shaderID, 500), Logger.LOG_STACK_TRACE);
			
			return -1;
		}
		
		return shaderID;
	}
	
	public static Shader LoadShader(String path)
	{
		Logger.writeLine("Loading Shader: " + path, Logger.LOG_DEBUG);	
		try 
		{
			Path filePath = Paths.get(path);		
			List<String> data = Files.readAllLines(filePath);
			
			StringWriter fragmentWriter = new StringWriter();
			StringWriter vertexWriter = new StringWriter();
			
			StringWriter current = fragmentWriter;
			for (String line : data) 
			{
				if(line.startsWith("frag:"))
				{
					current = fragmentWriter;
					line = line.substring(5, line.length());
				}
				else if (line.startsWith("vert:")) 	
				{
					current = vertexWriter;
					line = line.substring(5, line.length());
				}
				current.write("\n" + line);
			}
			
			String vertexData = vertexWriter.toString();
			String fragmentData = fragmentWriter.toString();
			
			fragmentWriter.close();
			vertexWriter.close();
			
			int vertexShader = compileShader(vertexData, GL_VERTEX_SHADER);
			int fragmentShader = compileShader(fragmentData, GL_FRAGMENT_SHADER);
			
			if(vertexShader != -1 && fragmentShader != -1)
			{
				return new Shader(vertexShader, fragmentShader);
			}
			
		} 
		catch (IOException e) 
		{
			Logger.writeLine(e);
		}
		
		
		return null;
	}
}
