package com.saltboxgames.latticelight.rendering;

import static org.lwjgl.glfw.GLFW.*;
import static com.saltboxgames.latticelight.utilities.Utilities.*;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_SHORT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glBufferSubData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import com.saltboxgames.latticelight.logging.Logger;

public class VertexBufferObject 
{
	private int indicesCount;
	private int id;
	private int 
		verticesBuffer_id,
		textureBuffer_id, 
		foreColorBuffer_id,
		backColorBuffer_id,
		indicesBuffer_id;
	
	public static final int 	
		VERTEX_ATTRIB 		= 0,
	 	FORE_COLOR_ATTRIB 	= 1,
		BACK_COLOR_ATTRIB 	= 2,
		TCOORD_ATTRIB 		= 3;
		
	private VertexBufferObject(float[] vertices, float[] texcoords, float[] colors, float[] backgroundColors, short[] indices)
	{
		id = glGenVertexArrays();
		glBindVertexArray(id);
		
		buildVertexBuffer(createFlippedBuffer(vertices));
		buildColorBuffer(createFlippedBuffer(colors));
		buildTexCoordBuffer(createFlippedBuffer(texcoords));
		buildBackgroundColorBuffer(createFlippedBuffer(backgroundColors));
		buildIndicesBuffer(createFlippedBuffer(indices));
		
		indicesCount = indices.length;
		
		glBindVertexArray(0);
	}
	
	private VertexBufferObject(FloatBuffer vertices, FloatBuffer texcoords, FloatBuffer colors, FloatBuffer backgroundColors, ShortBuffer indices)
	{
		id = glGenVertexArrays();
		glBindVertexArray(id);
		
		buildVertexBuffer(vertices);
		buildColorBuffer(colors);
		buildTexCoordBuffer(texcoords);
		buildBackgroundColorBuffer(backgroundColors);
		buildIndicesBuffer(indices);
		
		indicesCount = indices.capacity();
		
		glBindVertexArray(0);
	}
	
	public void Draw()
	{
		glBindVertexArray(id);
		//Enable Attributes
		glEnableVertexAttribArray(VERTEX_ATTRIB);
		glEnableVertexAttribArray(FORE_COLOR_ATTRIB);
		glEnableVertexAttribArray(BACK_COLOR_ATTRIB);
		glEnableVertexAttribArray(TCOORD_ATTRIB);
		
			
		glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_SHORT, 0);
		
		glDisableVertexAttribArray(VERTEX_ATTRIB);
		glDisableVertexAttribArray(FORE_COLOR_ATTRIB);
		glDisableVertexAttribArray(BACK_COLOR_ATTRIB);
		glDisableVertexAttribArray(TCOORD_ATTRIB);
		
		
		glBindVertexArray(0);
	}
	
	private void buildVertexBuffer(FloatBuffer vertices)
	{		
		verticesBuffer_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer_id);
		glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);	
		glVertexAttribPointer(VERTEX_ATTRIB, 3, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildTexCoordBuffer(FloatBuffer texcoords)
	{
		textureBuffer_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, textureBuffer_id);
		glBufferData(GL_ARRAY_BUFFER, texcoords, GL_DYNAMIC_DRAW);	
		glVertexAttribPointer(TCOORD_ATTRIB, 2, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildColorBuffer(FloatBuffer colors)
	{
		foreColorBuffer_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, foreColorBuffer_id);
		glBufferData(GL_ARRAY_BUFFER, colors, GL_DYNAMIC_DRAW);	
		glVertexAttribPointer(FORE_COLOR_ATTRIB, 3, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildBackgroundColorBuffer(FloatBuffer colors)
	{
		backColorBuffer_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, backColorBuffer_id);
		glBufferData(GL_ARRAY_BUFFER, colors, GL_DYNAMIC_DRAW);	
		glVertexAttribPointer(BACK_COLOR_ATTRIB, 3, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildIndicesBuffer(ShortBuffer indices)
	{
		indicesBuffer_id = glGenBuffers();

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer_id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);	
	}
	
	public void updateTexCoords(int start, FloatBuffer ntexCoords)
	{
		glBindBuffer(GL_ARRAY_BUFFER, textureBuffer_id);
		glBufferSubData(GL_ARRAY_BUFFER, start * 4, ntexCoords);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public void updateColor(int start, FloatBuffer nColor)
	{
		glBindBuffer(GL_ARRAY_BUFFER, foreColorBuffer_id);
		glBufferSubData(GL_ARRAY_BUFFER, start * 4, nColor);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public void updateBackColor(int start, FloatBuffer nColor)
	{
		glBindBuffer(GL_ARRAY_BUFFER, backColorBuffer_id);
		glBufferSubData(GL_ARRAY_BUFFER, start * 4, nColor);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public int getId() 
	{
		return id;
	}
	
	@Override
	protected void finalize() throws Throwable 
	{
		//Clean up OpenGL
		glDeleteBuffers(verticesBuffer_id);
		glDeleteBuffers(textureBuffer_id);
		glDeleteBuffers(foreColorBuffer_id);
		glDeleteBuffers(backColorBuffer_id);
		glDeleteBuffers(indicesBuffer_id);
		glDeleteVertexArrays(id);	
		super.finalize();
	}
	
	//-- Statics
	
	/**
	 * Creates a new VBO, returns null if There is no GL Context
	 * @param vertices
	 * @param texcoords
	 * @param colors
	 * @param backgroundColors
	 * @param indices
	 * @return Vertex Buffer Object if GL Context Exists.
	 */
	public static VertexBufferObject create(float[] vertices, float[] texcoords, float[] colors, float[] backgroundColors, short[] indices)
	{
		if(glfwGetCurrentContext() <= 0)
		{
			Logger.writeLine("Could not create VertexBufferObject, No GL Context!", Logger.LOG_DEBUG);
			return null;
		}
		return new VertexBufferObject(vertices, texcoords, colors, backgroundColors, indices);
	}
	
	/**
	 * Creates a new VBO, returns null if There is no GL Context
	 * @param vertices 
	 * @param texcoords
	 * @param colors
	 * @param backgroundColors
	 * @param indices
	 * @return Vertex Buffer Object if GL Context Exists.
	 */
	public static VertexBufferObject create(FloatBuffer vertices, FloatBuffer texcoords, FloatBuffer colors, FloatBuffer backgroundColors, ShortBuffer indices)
	{
		if(glfwGetCurrentContext() <= 0)
		{
			Logger.writeLine("Could not create VertexBufferObject, No GL Context!", Logger.LOG_DEBUG);
			return null;
		}
		return new VertexBufferObject(vertices, texcoords, colors, backgroundColors, indices);
	}
}
