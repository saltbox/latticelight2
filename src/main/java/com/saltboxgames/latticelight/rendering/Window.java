package com.saltboxgames.latticelight.rendering;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

import org.joml.Vector2i;

import com.saltboxgames.latticelight.logging.Logger;

//TODO: write more wrapper functions
public class Window 
{
	private long id;
	private String name;
	private Vector2i size;
	
	private Window(String name, long id, Vector2i size)
	{
		this.id = id;
		this.name = name;
		this.size = size;
	}
	
	public void MakeCurrent()
	{
		//TODO: this might be redundant, do some research.
		if(glfwGetCurrentContext() != id)
		{
			glfwMakeContextCurrent(id);
		}
	}
	
	public void SwapBuffers()
	{
		glfwSwapBuffers(id);
	}
	
	//TODO: Take Clear params
	public void Clear()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	//-- Setters
	public void setPosition(Vector2i pos)
	{
		glfwSetWindowPos(id, pos.x, pos.y);
	}
	
	
	//-- Getters
	public boolean shouldClose()
	{
		return glfwWindowShouldClose(id) != GLFW_FALSE;
	}
	
	public long getId() 
	{
		return id;
	}
	
	public Vector2i getSize() 
	{
		return size;
	}
	
	public int getWidth()
	{
		return size.x;
	}
	
	public int getHeight()
	{
		return size.y;
	}
	
	public String getName() 
	{
		return name;
	}
	
	//-- Static Creators
	public static Window createWindow(String title, Vector2i size)
	{
		return createWindow(title, size, NULL, NULL);
	}
	
	public static Window createWindow(String title, Vector2i size, Long monitor, Long share)
	{
		long id = glfwCreateWindow(size.x, size.y, title, monitor, share);
		
		if(id != NULL)
		{
			return new Window(title, id, size);
		}
		else 
		{
			Logger.writeLine("Failed to create window!", Logger.LOG_ERROR);
			return null;
		}
	}
}
