package com.saltboxgames.latticelight.rendering;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBImage.stbi_load;
import static org.lwjgl.stb.STBImage.stbi_set_flip_vertically_on_load;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.joml.Vector2i;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL13;

import com.saltboxgames.latticelight.logging.Logger;

public class Texture 
{
	private int id;
	private Vector2i size;
	
	private Texture(int id, Vector2i size)
	{
		this.id = id;
		this.size = size;
	}
	
	public int getId() 
	{
		return id;
	}
	
	public void bind(int TextureNum)
	{
		if(TextureNum >= 32)
		{
			return;
		}
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + TextureNum);
		glBindTexture(GL_TEXTURE_2D, id);
	}
	
	public Vector2i getSize() 
	{
		return new Vector2i(size);
	}
	
	public int getHeight() 
	{
		return size.y;
	}
	public int getWidth() 
	{
		return size.x;
	}
	
	//TODO: Write Finalize to clean up GPU Ram
	
	//-- Statics
	public static Texture LoadTexture(String path)
	{	
		Logger.writeLine("Loading Texture: " + path, Logger.LOG_DEBUG);	
		
		IntBuffer w = BufferUtils.createIntBuffer(1);
		IntBuffer h = BufferUtils.createIntBuffer(1);
		IntBuffer comp = BufferUtils.createIntBuffer(1);
		
		//load image
		stbi_set_flip_vertically_on_load(1);
		ByteBuffer imageData = stbi_load(path, w, h, comp, 4);
		if (imageData == null)
		{
			Logger.writeLine("Failed to load texture: " + path, Logger.LOG_ERROR);
			return null;
		}
		
		/* Get width and height of image */
	    int width = w.get();
	    int height = h.get(); 
	    int id = glGenTextures();

		glBindTexture(GL_TEXTURE_2D, id);
		
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	
	    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
		  
		return new Texture(id, new Vector2i(width, height));
	}
}
