package com.saltboxgames.latticelight.rendering;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import org.joml.Vector2i;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

public class TileMesh implements Mesh
{	
	protected boolean texCoordBufChange = false;
	protected boolean colorBufChange = false;
	protected boolean backColorBufChange = false;
	
	protected VertexBufferObject vbo;
	
	protected int width, height;	
	protected float halfWidth, halfHeight, tileWidth, tileHeight;
	
	protected float[] vertices;
	protected float[] texCoords;
	protected float[] colors;
	protected float[] backColors;
	protected short[] indices;
	
	public TileMesh(int width, int height, int  sheetWidthInTiles, int sheetHeightInTiles)
	{
		this.width = width;
		this.height = height;
		
		this.halfWidth =  (float) (width)  / 2f;
		this.halfHeight = (float) (height) / 2f;
		
		this.tileWidth = 1f / (float)sheetWidthInTiles;
		this.tileHeight = 1f / (float)sheetHeightInTiles;
		
		buildMesh();
		
		this.updateTileBuffer = BufferUtils.createFloatBuffer(8);
		this.updateColorBuffer = BufferUtils.createFloatBuffer(12);
	}
	
	public void Draw()
	{
		if(vbo != null)
		{
			vbo.Draw();
		}
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	protected void buildMesh()
	{
		int numVert = (width * height) * 4;
		
		FloatBuffer vertices = BufferUtils.createFloatBuffer(numVert * 3);
		FloatBuffer texCoords = BufferUtils.createFloatBuffer(numVert * 2);
		FloatBuffer colors 	 = BufferUtils.createFloatBuffer(numVert * 4);
		FloatBuffer backgroundColors 	 = BufferUtils.createFloatBuffer(numVert * 4);
		ShortBuffer indices  = BufferUtils.createShortBuffer(numVert * 6);
		
		float xPos, yPos;
		int squareVal;
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				squareVal = ((y * width + x) * 4);
				
				xPos = x - halfWidth;
				yPos = y - halfHeight;
				
				vertices.put(new float[]{xPos		, yPos     , 0 });
				vertices.put(new float[]{xPos + 1	, yPos	   , 0 });
				vertices.put(new float[]{xPos 		, yPos + 1 , 0 });
				vertices.put(new float[]{xPos + 1	, yPos + 1 , 0 });
				
				texCoords.put(new float[]{0 * tileWidth, -0 * tileHeight});
				texCoords.put(new float[]{1 * tileWidth, -0 * tileHeight});
				texCoords.put(new float[]{0 * tileWidth, -1 * tileHeight});
				texCoords.put(new float[]{1 * tileWidth, -1 * tileHeight});
				
				// Color			  R  G  B  A
				colors.put(new float[]{1, 1, 1});
				colors.put(new float[]{1, 1, 1});
				colors.put(new float[]{1, 1, 1});
				colors.put(new float[]{1, 1, 1});
				
				backgroundColors.put(new float[]{0, 0, 0});
				backgroundColors.put(new float[]{0, 0, 0});
				backgroundColors.put(new float[]{0, 0, 0});
				backgroundColors.put(new float[]{0, 0, 0});				
				
				indices.put((short)(squareVal + 0));
				indices.put((short)(squareVal + 1));
				indices.put((short)(squareVal + 2));
				//*/
				
				indices.put((short)(squareVal + 1));
				indices.put((short)(squareVal + 3));
				indices.put((short)(squareVal + 2));
				//*/
			}			
		}		

		vertices.rewind();
		texCoords.rewind();
		colors.rewind();
		backgroundColors.rewind();
		indices.rewind();
		
		this.vertices = new float[vertices.limit()];
		vertices.get(this.vertices);
		
		this.texCoords = new float[texCoords.limit()];
		texCoords.get(this.texCoords);
		
		this.colors = new float[colors.limit()];
		colors.get(this.colors);
		
		this.backColors = new float[backgroundColors.limit()];
		backgroundColors.get(this.backColors);
		
		this.indices = new short[indices.limit()];
		indices.get(this.indices);
		
		vertices.flip();
		texCoords.flip();
		colors.flip();
		backgroundColors.flip();
		indices.flip();
		
		this.vbo = VertexBufferObject.create(vertices, texCoords, colors, backgroundColors, indices);
	}
	
	// Push Buffers
		public void pushColorBuffer()
		{
			int numVert = (width * height) * 4;
			FloatBuffer colors = BufferUtils.createFloatBuffer(numVert * 4);
			
			colors.put(this.colors);
			colors.flip();
			
			colorBufChange = false;
			
			if(vbo != null)
			{
				this.vbo.updateColor(0, colors);
			}
		}
		
		public void pushBackColorBuffer()
		{
			int numVert = (width * height) * 4;
			FloatBuffer backColors = BufferUtils.createFloatBuffer(numVert * 4);
			
			backColors.put(this.backColors);
			backColors.flip();
			
			backColorBufChange = false;
			
			if(vbo != null)
			{
				this.vbo.updateBackColor(0, backColors);
			}
		}
		
		public void pushTexCoordBuffer()
		{
			int numVert = (width * height) * 4;
			FloatBuffer texCoords = BufferUtils.createFloatBuffer(numVert * 2);
			
			texCoords.put(this.texCoords);
			texCoords.flip();
			
			texCoordBufChange = false;
			
			if(vbo != null)
			{
				this.vbo.updateTexCoords(0, texCoords);
			}
		}
		
		public void pushBuffers()
		{
			if(texCoordBufChange)
			{
				pushTexCoordBuffer();
			}
			if(colorBufChange)
			{
				pushColorBuffer();
			}
			if(backColorBufChange)
			{
				pushBackColorBuffer();
			}
		}
		
		// Tile
		private FloatBuffer updateTileBuffer;
		public void setQuadTile(Vector2i pos, Vector2i tile)
		{
			if(pos.x < 0 || pos.x >= width || pos.y < 0 || pos.y >= height)
			{
				return;
			}
			
			//It's awful cause we want to update the buffer, but also only push this change.
			
			int startPos = ((pos.y * width + pos.x) * 8);
			updateTileBuffer.clear();
			updateTileBuffer.put(texCoords[startPos + 0] =  (tile.x + 0) * tileWidth);
			updateTileBuffer.put(texCoords[startPos + 1] = -(tile.y + 0) * tileHeight);
			updateTileBuffer.put(texCoords[startPos + 2] =  (tile.x + 1) * tileWidth);
			updateTileBuffer.put(texCoords[startPos + 3] = -(tile.y + 0) * tileHeight);	
			updateTileBuffer.put(texCoords[startPos + 4] =  (tile.x + 0) * tileWidth);
			updateTileBuffer.put(texCoords[startPos + 5] = -(tile.y + 1) * tileHeight);
			updateTileBuffer.put(texCoords[startPos + 6] =  (tile.x + 1) * tileWidth);
			updateTileBuffer.put(texCoords[startPos + 7] = -(tile.y + 1) * tileHeight);
			
			updateTileBuffer.flip();
			
			if(vbo != null)
			{
				vbo.updateTexCoords(startPos, updateTileBuffer);
			}
		}
		
		public void setQuadTile(Vector2i pos, byte tile)
		{
			int x = tile % 16;
			int y = tile / 16;	
			setQuadTile(pos, new Vector2i(x, y));
		}
		
		public void setQuadTileRow(Vector2i startPos, byte[] tiles)
		{
			//TODO: this is missing
		}
		
		public void setQuadTileRow(Vector2i startPos, Vector2i[] tiles)
		{
			//TODO: this is missing
		}
		
		
		// -- Color
		
		private FloatBuffer updateColorBuffer;
		public void setQuadColor(Vector2i pos, Vector3f color)
		{
			if(pos.x < 0 || pos.x >= width || pos.y < 0 || pos.y >= height)
			{
				return;
			}
			
			int startPos = ((pos.y * width + pos.x) * 12);
			
			updateColorBuffer.clear();
			updateColorBuffer.put(colors[startPos + 0]  = color.x);
			updateColorBuffer.put(colors[startPos + 1]  = color.y);
			updateColorBuffer.put(colors[startPos + 2]  = color.z);	
			updateColorBuffer.put(colors[startPos + 3]  = color.x);
			updateColorBuffer.put(colors[startPos + 4]  = color.y);
			updateColorBuffer.put(colors[startPos + 5]  = color.z);
			updateColorBuffer.put(colors[startPos + 6]  = color.x);
			updateColorBuffer.put(colors[startPos + 7]  = color.y);
			updateColorBuffer.put(colors[startPos + 8]  = color.z);	
			updateColorBuffer.put(colors[startPos + 9]  = color.x);
			updateColorBuffer.put(colors[startPos + 10] = color.y);
			updateColorBuffer.put(colors[startPos + 11] = color.z);
			
			updateColorBuffer.flip();
			
			if(vbo != null)
			{
				vbo.updateColor(startPos, updateColorBuffer);
			}
		}
		
		public void setQuadColor(Vector2i pos, Vector3f[] color)
		{
			//TODO: this is missing
		}
		
		// Background Color
		public void setQuadBackColor(Vector2i pos, Vector3f color)
		{
			if(pos.x < 0 || pos.x >= width || pos.y < 0 || pos.y >= height)
			{
				return;
			}
			
			int startPos = ((pos.y * width + pos.x) * 12);
			
			updateColorBuffer.clear();
			updateColorBuffer.put(backColors[startPos + 0]  = color.x);
			updateColorBuffer.put(backColors[startPos + 1]  = color.y);
			updateColorBuffer.put(backColors[startPos + 2]  = color.z);	
			updateColorBuffer.put(backColors[startPos + 3]  = color.x);
			updateColorBuffer.put(backColors[startPos + 4]  = color.y);
			updateColorBuffer.put(backColors[startPos + 5]  = color.z);
			updateColorBuffer.put(backColors[startPos + 6]  = color.x);
			updateColorBuffer.put(backColors[startPos + 7]  = color.y);
			updateColorBuffer.put(backColors[startPos + 8]  = color.z);	
			updateColorBuffer.put(backColors[startPos + 9]  = color.x);
			updateColorBuffer.put(backColors[startPos + 10] = color.y);
			updateColorBuffer.put(backColors[startPos + 11] = color.z);
			
			updateColorBuffer.flip();
			
			if(vbo != null)
			{
				vbo.updateBackColor(startPos, updateColorBuffer);
			}
		}
		
		//---- Buffer Changes
		
		//-- Tile
		public void bufQuadTile(int x, int y, Vector2i tile)
		{
			if(x < 0 || x >= width || y < 0 || y >= height)
			{
				return;
			}
			
			int startPos = ((y * width + x) * 8);
			
			this.texCoords[startPos + 0] =  (tile.x + 0) * tileWidth;
			this.texCoords[startPos + 1] = -(tile.y + 0) * tileHeight;		
			this.texCoords[startPos + 2] =  (tile.x + 1) * tileWidth;
			this.texCoords[startPos + 3] = -(tile.y + 0) * tileHeight;		
			this.texCoords[startPos + 4] =  (tile.x + 0) * tileWidth;
			this.texCoords[startPos + 5] = -(tile.y + 1) * tileHeight;		
			this.texCoords[startPos + 6] =	(tile.x + 1) * tileWidth;
			this.texCoords[startPos + 7] = -(tile.y + 1) * tileHeight;
			
			texCoordBufChange = true;
		}
		
		public void bufQuadTile(int x, int y, byte tile)
		{
			int xt = tile % 16;
			int yt = tile / 16;	
			bufQuadTile(x, y, new Vector2i(xt, yt));
		}
		
		public void bufQuadTile(int x, int y, char tile)
		{
			bufQuadTile(x, y, (byte) tile);
		}
		
		//-- Foreground Color
		public void bufQuadColor(int x, int y, Vector3f color)
		{
			if(x < 0 || x >= width || y < 0 || y >= height)
			{
				return;
			}
			
			int startPos = ((y * width + x) * 12);
			
			this.colors[startPos + 0] = color.x;
			this.colors[startPos + 1] = color.y;
			this.colors[startPos + 2] = color.z;		
			this.colors[startPos + 3] = color.x;
			this.colors[startPos + 4] = color.y;
			this.colors[startPos + 5] = color.z;	
			this.colors[startPos + 6] = color.x;
			this.colors[startPos + 7] = color.y;
			this.colors[startPos + 8] = color.z;	
			this.colors[startPos + 9] = color.x;
			this.colors[startPos + 10] = color.y;
			this.colors[startPos + 11] = color.z;
			
			colorBufChange = true;
		}
		
		public void bufQuadColor(int x, int y, Vector3f[] colors)
		{
			if(x < 0 || x >= width || y < 0 || y >= height)
			{
				return;
			}
			
			if(colors.length < 4)
			{
				return;
			}
			
			int startPos = ((y * width + x) * 12);
			
			this.colors[startPos + 0] = colors[0].x;
			this.colors[startPos + 1] = colors[0].y;
			this.colors[startPos + 2] = colors[0].z;
			
			this.colors[startPos + 3] = colors[1].x;
			this.colors[startPos + 4] = colors[1].y;
			this.colors[startPos + 5] = colors[1].z;
			
			this.colors[startPos + 6] = colors[2].x;
			this.colors[startPos + 8] = colors[2].y;
			this.colors[startPos + 8] = colors[2].z;
			
			this.colors[startPos + 9]  = colors[3].x;
			this.colors[startPos + 10] = colors[3].y;
			this.colors[startPos + 11] = colors[3].z;
			
			colorBufChange = true;
		}
		
		//-- Background Color	
		public void bufQuadBackColor(int x, int y, Vector3f color)
		{
			if(x < 0 || x >= width || y < 0 || y >= height)
			{
				return;
			}
			
			int startPos = ((y * width + x) * 12);
			
			this.backColors[startPos + 0] = color.x;
			this.backColors[startPos + 1] = color.y;
			this.backColors[startPos + 2] = color.z;		
			this.backColors[startPos + 3] = color.x;
			this.backColors[startPos + 4] = color.y;
			this.backColors[startPos + 5] = color.z;	
			this.backColors[startPos + 6] = color.x;
			this.backColors[startPos + 7] = color.y;
			this.backColors[startPos + 8] = color.z;	
			this.backColors[startPos + 9] = color.x;
			this.backColors[startPos + 10] = color.y;
			this.backColors[startPos + 11] = color.z;
			
			backColorBufChange = true;
		}
		
		public void bufQuadBackColor(int x, int y, Vector3f[] colors)
		{
			if(x < 0 || x >= width || y < 0 || y >= height)
			{
				return;
			}
			
			if(colors.length < 4)
			{
				return;
			}
			
			int startPos = ((y * width + x) * 12);
			
			this.backColors[startPos + 0] = colors[0].x;
			this.backColors[startPos + 1] = colors[0].y;
			this.backColors[startPos + 2] = colors[0].z;
			
			this.backColors[startPos + 3] = colors[1].x;
			this.backColors[startPos + 4] = colors[1].y;
			this.backColors[startPos + 5] = colors[1].z;
			
			this.backColors[startPos + 6] = colors[2].x;
			this.backColors[startPos + 8] = colors[2].y;
			this.backColors[startPos + 8] = colors[2].z;
			
			this.backColors[startPos + 9]  = colors[3].x;
			this.backColors[startPos + 10] = colors[3].y;
			this.backColors[startPos + 11] = colors[3].z;
			
			backColorBufChange = true;
		}
}
