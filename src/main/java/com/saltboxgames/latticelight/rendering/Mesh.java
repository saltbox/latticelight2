package com.saltboxgames.latticelight.rendering;

public interface Mesh 
{
	public void Draw();	
}
