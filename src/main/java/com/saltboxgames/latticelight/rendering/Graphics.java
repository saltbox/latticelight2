package com.saltboxgames.latticelight.rendering;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

public class Graphics 
{
	private Shader activeShader;
	private Texture activeTexture;

	private ViewPort viewPort;
	
	private Matrix4f modelMatrixWork, viewMatrixWork;
	
	public Graphics(ViewPort viewPort)
	{
		this.viewPort = viewPort;
		this.modelMatrixWork = new Matrix4f();
		this.viewMatrixWork = new Matrix4f();
	}
	
	public void Draw(Mesh mesh, Matrix4f projMatrix, Matrix4f viewMatrix, Matrix4f modelMatrix)
	{
		activeShader.Update(projMatrix, viewMatrix, modelMatrix);
		mesh.Draw();
	}
	
	public void Draw(Mesh mesh)
	{
		Draw(mesh, viewPort.getProjection(), viewMatrixWork.identity(), modelMatrixWork.identity());
	}
	
	public void Draw(Mesh mesh, Vector3f pos)
	{
		Draw(mesh, viewPort.getProjection(), viewMatrixWork.identity(), modelMatrixWork.identity().translate(pos));
	}
	
	public void Draw(Mesh mesh, Vector3f pos, Quaternionf rotation)
	{
		Draw(mesh, viewPort.getProjection(), viewMatrixWork.identity(), modelMatrixWork.identity().translate(pos).rotate(rotation));
	}
	
	public void DrawScreen(Mesh mesh, Vector3f pos)
	{
		Draw(mesh, viewPort.getProjection(), viewMatrixWork.identity(), modelMatrixWork.identity().translate(pos));
	}
	
	public void setTexture(Texture texture)
	{
		if(activeTexture != texture)
		{
			activeTexture = texture;
			texture.bind(0);
		}
	}
	
	public void setShader(Shader shader)
	{
		if(activeShader != shader)
		{
			activeShader = shader;
			shader.bind();
		}
	}
}
