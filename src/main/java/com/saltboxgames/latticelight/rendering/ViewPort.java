package com.saltboxgames.latticelight.rendering;

import org.joml.Matrix4f;
import org.joml.Vector2f;

public class ViewPort 
{
	private Window window;
	private Vector2f scale;
	
	float zNear, zFar;
	
	private Matrix4f perspective;
	
	public ViewPort(Window window, Vector2f scale, float zNear, float zFar)
	{
		this.perspective = new Matrix4f();
		
		this.window = window;
		this.scale = new Vector2f(1f / scale.x, 1f/ scale.y);
		this.zNear = zNear;
		this.zFar = zFar;
	}
	
	public Matrix4f getProjection()
	{
		return perspective.identity().setOrtho(0, (window.getWidth() * scale.x), (window.getHeight() * scale.y), 0, zNear, zFar);
	}
}
