package com.saltboxgames.latticelight;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import java.util.Map;

import junit.framework.Test;



import org.lwjgl.opengl.GL;
import org.lwjgl.glfw.GLFWVidMode;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.json.JSONArray;
import org.json.JSONObject;



import com.saltboxgames.latticelight.config.Config;
import com.saltboxgames.latticelight.config.ConfigSetting;
import com.saltboxgames.latticelight.config.LoadTypeHandler;
import com.saltboxgames.latticelight.enginetools.DebugView;
import com.saltboxgames.latticelight.enginetools.EngineTool;
import com.saltboxgames.latticelight.enginetools.EngineToolMode;
import com.saltboxgames.latticelight.input.CharCallbackHandler;
import com.saltboxgames.latticelight.input.Key;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.logging.GameConsole;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.logging.SystemConsole;
import com.saltboxgames.latticelight.rendering.Graphics;
import com.saltboxgames.latticelight.rendering.Shader;
import com.saltboxgames.latticelight.rendering.Texture;
import com.saltboxgames.latticelight.rendering.ViewPort;
import com.saltboxgames.latticelight.rendering.Window;
import com.saltboxgames.latticelight.utilities.IConsoleCommand;
import com.saltboxgames.latticelight.utilities.FrameCounter;import com.saltboxgames.latticelight.utilities.Utilities;


public abstract class Game 
{
	private String name;
	
	//-- Rendering
	private Window window;
	private ViewPort viewPort;
	private Graphics graphics;
	private Shader defaultShader;
    private Texture defautlTexture;
	
    private boolean noGUI;
    
	//-- Game Loop
	private boolean isRunning;
		
	//-- Commands
	private HashMap<String, IConsoleCommand> commands;
	
	//-- Logging
	private SystemConsole systemConsole;
	
	//-- Engine Tools
	private HashMap<String, EngineTool> engineTools;
	
	//-- Settings
	private HashMap<String, String[]> launchArgs;
	@ConfigSetting
	private Vector2i windowSize = new Vector2i(100, 50);
	@ConfigSetting
	private Vector2i tileSize	= new Vector2i(12, 12);
	@ConfigSetting
	private int vsyncMode = 1;
	@ConfigSetting(name = "framerate")
	private int tickRate = 60;
	
	
	public Game(String name, String[] args)
	{
		Logger.writeLine("LatticeLight:" + name);
		
		this.launchArgs = new HashMap<>();
		parseLaunchParams(args);
		
		Logger.writeLine("launch args: " + launchArgs.size(), Logger.LOG_DEBUG);
		
		Config.loadSettings(this, "latticelight.conf");
		
		this.name = name;
		this.commands = new HashMap<>();
		this.engineTools = new HashMap<>();		
		this.noGUI = launchArgs.containsKey("no-gui");
		
		if(!noGUI)
		{
			initGUI(new Vector2i(windowSize.x * tileSize.x, windowSize.y * tileSize.y));
			
			systemConsole = new SystemConsole(this);
			Logger.addLogger(systemConsole);
			
			addEngineTool("fps-counter", new FrameCounter(this, new Vector3f(windowSize.x - 4, 0.5f, 99)));
			
			addEngineTool("debug-view", DebugView.getInstance());
			
			Vector2f consolePos = new Vector2f(windowSize.x / 2f, windowSize.y / 2f / 2f);		
			GameConsole gameConsole = new GameConsole(this, consolePos, new Vector2i(windowSize.x, windowSize.y / 2));
			
			KeyboardInput.registerKeyCallBack(gameConsole);
			CharCallbackHandler.registerCallBack(gameConsole);
			Logger.addLogger(gameConsole);
			
			addEngineTool("game-console", gameConsole);
		}
		else 
		{
			initNoGUI();
		}
		
		initEngineCommands();
	}
	
	private void initGUI(Vector2i size)
	{
		// init GLFW
		if(glfwInit() != GL_TRUE)
		{
			//FAILED TO INTI GLEW!
			Logger.writeLine("GLEW initialization failed", Logger.LOG_ERROR);
			return;
		}
		
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		
		GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		// init Default Window
		window = Window.createWindow(name, size);
		if(window == null)
		{
			Logger.writeLine("Window creation failed", Logger.LOG_ERROR);
			return;
		}
		
		//TODO: Load this from a config file, store last position
		Vector2i windowPos = new Vector2i((vidMode.width() - window.getSize().x) / 2, (vidMode.height() - window.getSize().y) / 2);
		window.setPosition(windowPos);
		
		window.MakeCurrent();
		
		GL.createCapabilities();
		Logger.writeLine("OpenGL: " + glGetString(GL_VERSION), Logger.LOG_DEBUG);
		
		glEnable(GL_TEXTURE_2D);
        glEnable(GL_DEPTH_TEST);
		
        glClearColor(0.392f, 0.584f, 0.929f, 1.0f);
        glClearColor(0, 0, 0, 1);
        
        // Register Inputs to Context
        glfwSetCharCallback(window.getId(), CharCallbackHandler.getInstance());
        glfwSetKeyCallback(window.getId(), KeyboardInput.getInstance());
        MouseInput.getInstance().registerCallBacks(window.getId());
        
        glfwSwapInterval(vsyncMode);
        
        //Load Defaults
        defaultShader = Shader.LoadShader("data/shaders/default.shdr");
        defautlTexture = Texture.LoadTexture("data/ASCII.png");
        
        //TODO: Make sure setting is found, and not null
        viewPort = new ViewPort(window, new Vector2f(tileSize.x, tileSize.y), -100, 100);
        graphics = new Graphics(viewPort);  
	}
	
	private void initNoGUI()
	{
		systemConsole = new SystemConsole(this);
		Logger.addLogger(systemConsole);
	}

	public abstract void onStart();
	public abstract void onStop();
	public abstract void onUpdate(GameTime gameTime);
	public abstract void onDraw(GameTime gameTime, Graphics graphics);
	
	public void Start()
	{
		onStart();
		isRunning = true;
		executeLaunchCommands();
		run();
	}
	
	public void Stop()
	{
		isRunning = false;
		onStop();
	}
	
	// -- Game Loop
	private void run()
	{		
		GameTime gameTime = new GameTime();
		double deltaTime;
		
		sync(tickRate);
		sync(tickRate);
		while (isRunning) 
		{
			if(!noGUI)
    		{
				KeyboardInput.Update();
				MouseInput.Update();
				glfwPollEvents();
	        	if(window.shouldClose())
	        	{
	        		Stop();
	        	}
    		}
			
			deltaTime = updateLength / 1000000000.0d;
			gameTime.setDeltaTime(deltaTime);
			
			boolean haultGame = updateEngineTools(gameTime);
			
			//-UPDATE
			if(!haultGame)
			{
				onUpdate(gameTime);
			}
			
			if(!noGUI)
			{
				graphics.setShader(defaultShader);
		        graphics.setTexture(defautlTexture);
		        
		        //-DRAW
		        for (EngineTool engineTool : engineTools.values()) 
				{
		        	if(engineTool.isActive())
		        	{
		        		engineTool.onDraw(gameTime, graphics);
		        	}
				}
		        
		        onDraw(gameTime, graphics);
		        
		        if(vsyncMode == 0)
		        {
		        	sync(tickRate);
		        }
		        else 
		        {
		        	sync(0);
				}
		        			
				window.MakeCurrent();
				window.SwapBuffers();
				window.Clear();
			}
			else // -- NO GUI
			{
				sync(tickRate);
			}
		}
	}

	private boolean updateEngineTools(GameTime gameTime) 
	{
		//SPECIAL CASE: handle enable and disable of game-console. 
		if(KeyboardInput.isKeyDown(Key.GRAVE_ACCENT) && !KeyboardInput.isKeyDown(Key.GRAVE_ACCENT, true))
		{
			EngineTool gameConsole =  engineTools.get("game-console");
			if(gameConsole != null)
			{
				if(gameConsole.isActive())
				{
					gameConsole.mode = EngineToolMode.INACTIVE;
				}
				else 
				{
					gameConsole.mode = EngineToolMode.ACTIVE;
					
					if(KeyboardInput.isKeyDown(Key.LEFT_SHIFT))
					{
						gameConsole.mode = EngineToolMode.ACTIVE_IGNORE_INPUT;
					}
					if(KeyboardInput.isKeyDown(Key.LEFT_CONTROL))
					{
						gameConsole.mode = EngineToolMode.ACTIVE_HAULT_UPDATE;
					}
				}
			}
		}
		
		//-UPDATE TOOLS
		boolean haultGame = false;
		for (EngineTool engineTool : engineTools.values()) 
		{
			if(engineTool.isActive())
			{
				if(engineTool.shouldUpdateInput())
				{
					engineTool.onUpdateInput(gameTime);
				}
				engineTool.onUpdate(gameTime);
				if(engineTool.haultGame())
				{
					haultGame = true;
				}
			}
		}
		return haultGame;
	}
	
	
	private long variableYieldTime, lastTime, updateLength;
	 /**
     * An accurate sync method that adapts automatically
     * to the system it runs on to provide reliable results.
     * 
     * @param fps The desired frame rate, in frames per second
     * @author kappa (On the LWJGL Forums) http://forum.lwjgl.org/index.php?topic=4452.0
     */
	private void sync(int frameRate) 
    {
    	if (frameRate <= 0) 
    	{
    		updateLength = System.nanoTime() - lastTime;
    		lastTime = System.nanoTime();
    		return;
		}
    	
        long sleepTime = 1000000000 / frameRate; // nanoseconds to sleep this frame
        // yieldTime + remainder micro & nano seconds if smaller than sleepTime
        long yieldTime = Math.min(sleepTime, variableYieldTime + sleepTime % (1000*1000));
        long overSleep = 0; // time the sync goes over by
          
        try 
        {
            while (true) 
            {
            	updateLength = System.nanoTime() - lastTime;
                  
                if (updateLength < sleepTime - yieldTime) 
                {
                    Thread.sleep(1);
                }
                else if (updateLength < sleepTime) 
                {
                    // burn the last few CPU cycles to ensure accuracy
                    Thread.yield();
                }
                else 
                {
                    overSleep = updateLength - sleepTime;
                    break; // exit while loop
                }
            }
        }
        catch (InterruptedException e) 
        {
            Logger.writeLine(e);
        }
        finally
        {
            lastTime = System.nanoTime() - Math.min(overSleep, sleepTime);
             
            // auto tune the time sync should yield
            if (overSleep > variableYieldTime) 
            {
                // increase by 200 microseconds (1/5 a ms)
                variableYieldTime = Math.min(variableYieldTime + 200*1000, sleepTime);
            }
            else if (overSleep < variableYieldTime - 200*1000) 
            {
                // decrease by 2 microseconds
                variableYieldTime = Math.max(variableYieldTime - 2*1000, 0);
            }
        }
    }
	
	// Command Handling
	public void registerCommand(String name, IConsoleCommand com)
	{
		if(!commands.containsKey(name.toLowerCase()))
		{
			commands.put(name.toLowerCase(), com);
		}
	}
	
	public boolean executeCommand(String name, String[] args)
	{
		IConsoleCommand com = commands.get(name.toLowerCase());
		if(com != null)
		{
			com.run(args);
			return true;
		}
		return false;
	}
	
	public boolean executeCommand(String name, String arg0)
	{
		return executeCommand(name, new String[]{arg0});
	}
	
	public boolean executeCommand(String name)
	{
		return executeCommand(name, new String[0]);
	}
	
	// Engine tool handling
	public void addEngineTool(String name, EngineTool engineTool)
	{
		commands.put("ll." + name.toLowerCase(), (String[] args) ->
		{
			if (args.length >= 1) 
			{
				if(Utilities.canParseInt(args[0]))
				{
					int mode = Integer.parseInt(args[0]);
					if(mode >= 0 && mode < EngineToolMode.values.length)
					{
						engineTool.mode = EngineToolMode.values[mode];
					}
					else
					{
						//TODO OUT OF RANGE
					}
				}
			}
			else 
			{
				//TODO: NOT ENOUGH ARGS
			}
		});	
		engineTools.put(name.trim().toLowerCase(), engineTool);
	}
	
	public void removeEngineTool(String name)
	{
		engineTools.remove(name.trim().toLowerCase());
	}
	
	public EngineTool getEngineTool(String name)
	{
		return engineTools.get(name.trim().toLowerCase());
	}
	
	// Launch params handling
	private void parseLaunchParams(String[] args)
	{
		ArrayList<String> currentParams = new ArrayList<String>();
		String currentArg = "";
		for(int i = 0; i < args.length; i ++)
		{
			if(args[i].startsWith("--"))
			{
				String nextArg = args[i].substring(2, args[i].length());
				if(currentArg.length() > 0)
				{
					String[] params = currentParams.toArray(new String[currentParams.size()]);
					launchArgs.put(currentArg, params);
				}
				currentArg = nextArg;
			}
			else if (currentArg.length() > 0) 
			{
				currentParams.add(args[i]);
			}
		}
		
		if(currentArg.length() > 0)
		{
			String[] params = currentParams.toArray(new String[currentParams.size()]);
			launchArgs.put(currentArg, params);
		}
	}
	
	private void executeLaunchCommands()
	{
		for (Map.Entry<String, String[]> launchArg : launchArgs.entrySet()) 
		{
			executeCommand(launchArg.getKey(), launchArg.getValue());
		}
	}
	
	//-- Getters
	public int getTickRate() {
		return tickRate;
	}
	
	
	// -- Engine Commands
	private void initEngineCommands()
	{
		
	}
}
