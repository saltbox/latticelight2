package com.saltboxgames.latticelight.enginetools;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME) 
public @interface DebugVariable 
{
	
}
