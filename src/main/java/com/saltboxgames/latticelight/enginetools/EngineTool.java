package com.saltboxgames.latticelight.enginetools;

import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.rendering.Graphics;

public abstract class EngineTool 
{	
	public EngineToolMode mode;
	
	public EngineTool()
	{
		mode = EngineToolMode.INACTIVE;
	}
	
	public void onUpdateInput(GameTime gameTime)
	{
		
	}
	
	public void onUpdate(GameTime gameTime)
	{
		
	}
	
	public void onDraw(GameTime gameTime, Graphics graphics)
	{
		
	}
	
	public boolean shouldUpdateInput()
	{
		return mode.compareTo(EngineToolMode.ACTIVE) >= 0;
	}
	
	public boolean isActive()
	{
		return mode.compareTo(EngineToolMode.INACTIVE) > 0;
	}
	
	public boolean haultGame()
	{
		return mode.compareTo(EngineToolMode.ACTIVE_HAULT_UPDATE) == 0;
	}
}
