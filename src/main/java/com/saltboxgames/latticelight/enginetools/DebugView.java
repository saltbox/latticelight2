package com.saltboxgames.latticelight.enginetools;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.joml.Vector2f;
import org.joml.Vector3f;


import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.rendering.Graphics;
import com.saltboxgames.latticelight.utilities.IDataFormatter;

public class DebugView extends EngineTool 
{
	List<DebugPanel> debugPanels;
	
	
	private DebugView()
	{
		debugPanels = new ArrayList<>();
	}
	
	@Override
	public void onUpdate(GameTime gameTime) 
	{
		for (DebugPanel debugPanel : debugPanels) {
			debugPanel.update();
		}
	}
	
	@Override
	public void onDraw(GameTime gameTime, Graphics graphics) 
	{
		for (DebugPanel debugPanel : debugPanels) {
			debugPanel.draw(graphics);
		}
	}
	
	private DebugPanel _put(Object target, String name, Vector2f position, int width, boolean inWorld)
	{
		DebugPanel dp = new DebugPanel(name, new DebugData(target), position, width, inWorld);
		debugPanels.add(dp);
		return dp;
	}
	
	// Static	
	protected static HashMap<Class<?>, IDataFormatter> dataFormatters = new HashMap<>();
	
	static
	{
		dataFormatters.put(Vector3f.class, (Object object) ->
		{
			Vector3f vec3 = (Vector3f) object;
			if(vec3 == null)
			{
				return new String[3];
			}
			return new String[] { "X: " + vec3.x, "Y: " + vec3.y, "Z: " + vec3.z };
		});
	}
	
	
	public static DebugView instance;
	public static DebugView getInstance()
	{
		if(instance == null)
		{
			instance = new DebugView();
		}
		return instance;
	}
	
	public static DebugPanel put(Object target, String name, Vector2f position, int width, boolean inWorld)
	{
		return getInstance()._put(target, name, position, width, inWorld);
	}
	
	public static DebugPanel put(Object target, String name, Vector2f position, int width)
	{
		return put(target, name, position, width, false);
	}
	
	public static DebugPanel put(Object target, String name, Vector2f position)
	{
		return put(target, name, position, 15);
	}
	
	public static DebugPanel put(Object target, String name)
	{
		return put(target, name, new Vector2f(9f, 25));
	}
	
	public static DebugPanel put(Object target, Vector2f position)
	{
		return put(target, target.getClass().getSimpleName(), position);
	}
	
	public static DebugPanel put(Object target)
	{
		return put(target, target.getClass().getSimpleName());
	}
	
	protected class DebugData
	{
		Object target;
		Field[] fields;
			
		private DebugData(Object target) 
		{
			this.target = target;
			
			List<Field> targetFields = new ArrayList<Field>();
			
			Class current = target.getClass();
			do {
				Field[] fields = target.getClass().getDeclaredFields();
				for(Field field : fields)
				{
					DebugVariable debugVariable = field.getAnnotation(DebugVariable.class);
					if(debugVariable != null)
					{
						targetFields.add(field);
					}
				}
				current = current.getSuperclass();
			} while (current != null && current != Object.class);
			
			this.fields = targetFields.toArray(new Field[targetFields.size()]);
		}
	}
}
