package com.saltboxgames.latticelight.enginetools;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltboxgames.latticelight.enginetools.DebugView.DebugData;
import com.saltboxgames.latticelight.rendering.Graphics;
import com.saltboxgames.latticelight.rendering.TileMesh;
import com.saltboxgames.latticelight.utilities.IDataFormatter;

class DebugPanel
{
	private TileMesh mesh;
	private String name;
	private boolean inWorld;
	private Vector3f position;
	
	private DebugData data;

	private int width;
	
	public DebugPanel(String name, DebugData data, Vector2f position, int width, boolean inWorld) 
	{
		this.position = new Vector3f(position, 49.5f);
		this.inWorld = inWorld;			
		this.data = data;
		this.name = name;
		this.width = width;
		
		this.mesh = new TileMesh(width, 1, 16, 16);
		bufData();
	}
	
	protected void update()
	{
		bufData();
	}
	
	protected void draw(Graphics graphics)
	{
		this.mesh.pushBuffers();
		if(inWorld)
		{
			graphics.Draw(mesh, position);
		}
		else 
		{
			graphics.DrawScreen(mesh, position);
		}
	}
	
	private void bufData()
	{
		bufString(name, 0);
		int lineNum = 1;
		for (Field field : data.fields) 
		{
			bufString(field.getName() + ":", lineNum);
			lineNum ++;
			try 
			{	
				if((field.getModifiers() & Modifier.PRIVATE) == Modifier.PRIVATE)
				{
					field.setAccessible(true);
				}
				
				Object targetData = field.get(data.target);
				if(targetData != null)
				{
					IDataFormatter dataFormatter = DebugView.dataFormatters.get(field.getType());
					if(dataFormatter != null)
					{
						String[] format = dataFormatter.format(field.get(data.target));
						for (int i = 0; i < format.length; i++) 
						{
							bufString(" " +format[i], lineNum);
							lineNum ++;
						}	
					}
					else 
					{
						bufString(" " + field.get(data.target).toString(), lineNum);
						lineNum ++;
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			if(lineNum > mesh.getHeight())
			{
				mesh = new TileMesh(width, lineNum, 16, 16);
			}
		}
	}
	
	private void bufString(String string, int y)
	{
		for (int i = 0; i < string.length(); i++) 
		{
			if(i >= mesh.getWidth())
			{
				break;
			}
			else 
			{
				mesh.bufQuadTile(i, y, string.charAt(i));
			}
		}
	}
	
	// -- Setters
	public void setPosition(Vector2f position) 
	{
		this.position.x = position.x;
		this.position.y = position.y;
	}
}
