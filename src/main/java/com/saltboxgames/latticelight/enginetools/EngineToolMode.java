package com.saltboxgames.latticelight.enginetools;

public enum EngineToolMode 
{
	INACTIVE,
	ACTIVE_IGNORE_INPUT,
	ACTIVE,
	ACTIVE_HAULT_UPDATE;
	
	/**
	 * Static Cached values(), as a minor optimization for people making a lot of calls
	 */
	public static EngineToolMode[] values = values();
}
